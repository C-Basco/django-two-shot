from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.form import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required
def show_receipt(request):
    receipt = Receipt.objects.filter(purchaser=request.user.is_authenticated)
    context = {
        "receipt_object": receipt,
    }
    return render(request, "receipts/receipt.html", context)


# Forms
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()

            return redirect("receipts/")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()

            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()

            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)


# Lists


@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "expense_object": expense,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)

    context = {
        "account_object": account,
    }
    return render(request, "receipts/accounts.html", context)
